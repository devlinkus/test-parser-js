var fs = require('fs');
var vm = require('vm');
var path = require('path');
var acorn = require('acorn');

const ASTTool = require('./class/ASTTool.js');


/*
 * CATEGORY et le nom de la catégorie du cours et représente
 * le dossier où seront stocker les exos et leur correction, respectivement
 * dans "js_exo" et "js_check".
 *
 * Ex: exo n°1 de la catégorie Demo:
 * ./Cours/Demo/js_exo/exo1.js (l'exercice)
 * ./Cours/Demo/js_check/exo1.js (la correction)
 */
const CATEGORY = 'Demo';
const hasDom = false; // exo utilisant le dom
const exohtml = 1;
const exo = 1;


const raw = []; // raw script (ex script.js, index.html)

raw['script.js'] = fs.readFileSync('./Cours/'+CATEGORY+'/js_exo/exo'+exo+'.js', 'utf8');
const AST = acorn.parse(raw['script.js'], {ranges: false});
const exec = require('./Cours/'+CATEGORY+'/js_check/exo'+exo+'.js');

if(hasDom)
{
    raw['index.html'] = fs.readFileSync('./Cours/'+CATEGORY+'/js_exo/dom'+exohtml+'.html', 'utf8')
    const jsdom = require("jsdom");
    const { JSDOM } = jsdom;
    var { document } = ( new JSDOM(raw['index.html'])).window;
    global.document = document;
}
else
{
    var document = null;
}

vm.runInThisContext(fs.readFileSync(__dirname + '/Cours/'+CATEGORY+'/js_exo/exo'+exo+'.js'));

exec(AST, ASTTool, raw);