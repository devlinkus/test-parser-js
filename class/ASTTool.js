class ASTTool
{
    constructor(AST, props = [])
    {
        for(let i = 0; i < props.length; i++)
        {
            this._removeMetaFromAST(AST, props[i]);
        }

        this.AST = AST;
        this.errMsg = false;
    }

    /**
     * Return global AST as JSON;
     *
     * @returns {string}
     */
    getJSONAST(AST = null)
    {
        return AST === null ? JSON.stringify(this.AST) : JSON.stringify(AST);
    }


    /**
     * Count number of nodes in the ast at root level.
     *
     * @param realCount The expected number of nodes.
     * @param displayCount The number to display.
     * @returns {boolean}
     */
    countNode(realCount, displayCount = false)
    {
        if(!displayCount)
        {
            displayCount = realCount;
        }

        if(this.AST.body.length !== realCount)
        {
            this.errMsg = "Vous devez ajouter uniquement "+displayCount+" instruction"+ (displayCount > 1 ? 's' : '');
            this.errMsg += (displayCount !== realCount ? ' au script de départ' : '.');
            return false;
        }
        return true;
    }



    /**
     * Check if the body has required node.
     *
     * @param stringNode
     * @param AST
     */
    hasNode(stringNode, position = false)
    {
        for(let node of this.AST.body)
        {
            if(JSON.stringify(node) == JSON.stringify(stringNode))
            {
                return true;
            }
        }

        this.errMsg = "Vous ne devez pas modifier le script de départ. Appuyez sur le bouton reset pour réinitialiser le script.";

        return false
    }

    /**
     * Check if a variable is declared.
     *
     * @param varName
     * @param nodePosition
     * @returns {boolean}
     */
    hasVarDeclaration(varName, nodePosition = false)
    {
        let hasVariableDeclaration = false;
        let hasVar = false;
        let position = 0;
        let declarPosition = 0;
        let assignPosition = false;
        let myNode = false;
        for(let node of this.AST.body)
        {
            position++;
            if(node.type === 'VariableDeclaration')
            {
                hasVariableDeclaration = true;
                if(node.declarations[0].hasOwnProperty('id') && node.declarations[0].id.type === 'Identifier')
                {
                    if(node.declarations[0].id.name === varName)
                    {
                        if(hasVar)
                        {
                            this.errMsg = "Vous devez déclarer une seule fois la variable '"+varName+"'.";
                            return false;
                        }
                        hasVar = true;
                        myNode = node;
                        declarPosition = position;
                    }
                }
            }

            if(node.type === 'ExpressionStatement')
            {
                if(node.expression.hasOwnProperty('left') &&
                node.expression.left.hasOwnProperty('name') &&
                node.expression.left.name === varName)
                {
                    assignPosition = position;
                }
            }



        }
        if(!hasVariableDeclaration)
        {
            this.errMsg = "Vous n'avez pas déclaré de variable.";
            return false;
        }
        if(!hasVar)
        {
            this.errMsg = "Vous n'avez pas déclaré la variable '"+varName+"'.";
            return false;
        }
        if(assignPosition !== false && declarPosition > assignPosition)
        {
            this.errMsg = "Vous avez un assignement de la variable '"+varName+"' avant sa déclaration.";
            return false;
        }

        if(hasVariableDeclaration && hasVar && nodePosition !== false)
        {
            if(declarPosition !== nodePosition)
            {
                this.errMsg = "Vous aves bien déclaré la variable '"+varName+"' mais cette déclaration doit être votre instruction n°"+nodePosition+".";
                return false;
            }
        }
        return myNode;
    }

    hasFuncDeclaration(varName)
    {
        for(let node of this.AST.body)
        {
            if(node.type === 'FunctionDeclaration')
            {
                if(node.id.hasOwnProperty('name') && node.id.name === varName)
                {
                    return node;
                }
            }
        }

        this.errMsg = "Vous n'avez pas déclaré la fonction "+varName+".";
        return false;
    }

    /**
     * Check parts of a node.
     *
     * strs as:
     *      objet_sring - error_message - value_of_object_string
     *
     * [
     *  ['this.is.a.node', 'error msg' 'node value 'optional'], ...
     * ]
     *
     * in object_string:
     *  - if 'arr' is array in foo.bar.arr.prop.value
     *      then: value is foo.bar.arr[0].prop.value
     *  - if 'arr' is array in foo.bar.arr.prop-2.value
     *      then prop is foo.bar.arr[2].prop.value
     * in error_message:
     *  - if is array of error_message
     *      then error_message is launched if arr[2] not found
     *
     * @param strs
     * @param ast
     * @returns {boolean}
     */
    checkAST(strs = [], ast = null)
    {
        let AST = this._getAST(ast);
        let lastValue = null
        for(let str of strs)
        {
            let stat = this._checkNode(AST, str[0], str[1], str.length === 3 ? str[2] : undefined);
            if(stat === false)
            {
                return false;
            }
            else
            {
                lastValue = stat;
            }
        }
        return lastValue;
    }

    /**
     * Check if a script is equal to the model.
     *
     * @param modelPage
     * @param userPage
     * @returns {boolean}
     */
    checkModel(modelPage, userPage)
    {
        let m = modelPage.toString().replace(/\s/g, '').trim();
        let u = userPage.toString().replace(/\s/g, '').trim();

        if(m === u)
        {
            return true;
        }
        this.errMsg = 'Vous ne devez pas modifier le fichier index.html. Appuyez sur reset pour réinitialiser le script.';
        return false;
    }

    /**
     * Compare Object or Json;
     *
     * @param modelAST
     * @param userAST
     * @param isModelJSON
     * @param isUserJSON
     * @returns {boolean}
     */
    compareASTPart(modelAST, userAST, isModelJSON = false, isUserJSON = false)
    {
        let m = isModelJSON ? modelAST : JSON.stringify(modelAST);
        let u = isUserJSON ? userAST : JSON.stringify(userAST);

        return m === u;
    }

    /**
     * Check objet type.
     *
     * @param object The object to test.
     * @param {array|string} expectedType List of valide type.
     * @param {string} varName Name of the variable tested.
     * @returns {boolean}
     */
    testType(object, expectedType, varName)
    {
        let types = typeof expectedType === 'string' ? [expectedType] : expectedType;

        let rawObjType = Object.prototype.toString.call(object);
        let objType = rawObjType.replace('[object ', '').replace(']', '');

        for(let i = 0; i < types.length; i++)
        {
            if(types[i] === objType)
            {
                return true;
            }
        }

        let m = "La variable '"+varName+"' devrait être de type "+(types.join(' ou '))+". Elle est du type: "+objType+".";
        this.errMsg = m;
        return false;
    }


    findInFile(strings = [], file)
    {
        for(let i = 0; i < strings.length; i++)
        {
            if(file.includes(strings[i][0]) === false)
            {
                this.errMsg = strings[i][1];
                return false;
            }
        }
        return true;
    }



    /**
     * Return AST if given else global AST.
     *
     * @param {object} ast
     * @returns {*}
     * @private
     */
    _getAST(ast = null)
    {
        return ast === null ? this.AST.body : ast;
    }



    /**
     * Remove recursively properties from an object.
     *
     * @param {object} obj
     * @param {string} propName Property to remove.
     * @private
     */
    _removeMetaFromAST(obj, propName)
    {
        for(let prop in obj)
        {
            if (prop === propName)
            {
                delete obj[prop];
            }
            else if (typeof obj[prop] === 'object')
            {
                this._removeMetaFromAST(obj[prop], propName);
            }
        }
    }

    /**
     * Check part of a node
     * @param AST
     * @param str
     * @param errMsg
     * @param value
     * @returns {boolean}
     * @private
     */
    _checkNode(AST, str, errMsg, value)
    {
        let indiceMsg = errMsg;
        if(Array.isArray(errMsg))
        {
            indiceMsg = errMsg[1];
            errMsg = errMsg[0];
        }

        let props = str.split('.');
        let astCheck = AST;
        for(let prop of props)
        {
            let proptest = prop.split('-');
            let indice;
            if(proptest.length === 2 && typeof parseInt(proptest[1]) === 'number')
            {
                prop = proptest[0];
                indice = parseInt(proptest[1]);
            }

            if(Array.isArray(astCheck))
            {
                if (typeof indice !== "undefined") {
                    astCheck = astCheck[indice];
                    if(typeof astCheck === 'undefined')
                    {
                        this.errMsg = indiceMsg;
                        return false;
                    }
                }
                else
                {
                    astCheck = astCheck[0];
                }


            }

            if(astCheck !== null && astCheck.hasOwnProperty(prop))
            {
                astCheck = astCheck[prop];
            }
            else
            {
                this.errMsg = errMsg;
                return false;
            }
        }

        if(value !== undefined)
        {
            if(value !== astCheck)
            {
                this.errMsg = errMsg;
                return false;
            }
        }

        return astCheck;
    }


}


module.exports = ASTTool;