/* Expected PHP code


 */

function exec(myAST, ASTTool, raw)
{
    var errors = [];
    var warnings = [];
    var resultats;


    /*
     *  On charge l'AST dans la classe ASTTool.
     *  A ce stade, l'ast du code fournit par l'user est déjà généré.
     */
    let t = new ASTTool(myAST, ['start', 'end', 'raw']);

    /*
     * On travail UNIQEMENT entre les commentaires START SCRIPT et END SCRIPT
     * Ensuite, on copie le code dans le dans une copie du fichier exomodel.js.
     * Ce sera le fichier à uploader dans l'onglet 'Correction'
     *
     * Pour cette exemple on va analyser le code de ./Cours/Demo/js_exo/exo1.js
     *
     */


    /////// START SCRIPT ///////

    /*
    Ce code sert juste à afficher l'AST dans la console.
    C'est utilise de le coper coller dans
    https://maxleiko.github.io/json-query-tester/
    par exemple pour visualiser l'AST sur lequel on va travailler
     */

    // console.log(t.getJSONAST());


    /**
     * Etape 1:
     * On verifie que la variable est bien décalrer:
     */

    let nodeA = t.hasVarDeclaration('a');
    if(nodeA)
    {
        /**
         * Explication: let nodeA = t.hasVarDeclaration('a');
         * vérifie si une variable 'a' a été déclaré
         * dans le scope global. Si c'est le cas, nodeA a pour valeur
         * le fragment de l'AST correspondant à notre déclaration. Sinon
         * nodeA vaut false.
         *
         * Dans le cas où nodeA vaut false, un message d'erreur est généré.
         * On a juste à push l'erreur dans la variable 'errors'.
         *
         * NOTE IMPORTANTE: Dans la plupart des cas, les méthodes de
         * la classe ASTTool renvoient:
         * - un fragment d'AST qu'on peut tester avec ASTTool
         * - false, accompagné d'un message d'erreur dans la prop
         * errMsg de l'objet.
         */

        /**
         * Etape 2:
         * Verifier le contenu de la variable 'a'
         */

        let contentNodeA = t.checkAST([
            ['declarations-0.init.type', "Vous la variable a, vous devez afficher un littérale", 'Literal'],
            ['declarations-0.init.value', "Vous n'avez pas assigné la bonne valeur à la variable a", 42],
        ], nodeA);
        if(contentNodeA)
        {
            /**
             * Explication: on utilise la méthode checkAST() (la méthode la plus importante)
             * qui a 2 paramètres:
             * 1): un tableau de tableaux qui vont parser un fragment d'AST.
             *  - chaque tableau est constuté de 2 ou 3 éléments:
             *      - 1er élement: la propriété à trouver
             *      - 2eme element: le message d'erreur si:
             *          - la prop est introuvable
             *          - la valeur de la prop est incorrecte (si le 3eme ele est fournit)
             *      - 3eme élement (optionel): une valeur à comparé à la prop
             * 2): le noeud à analyser (dans notre cas c'est le noeud 'nodeA')
             */

            /*
            Voici l'ast de nodeA:

            {
              "type": "VariableDeclaration",
              "declarations": [
                {
                  "type": "VariableDeclarator",
                  "id": {
                    "type": "Identifier",
                    "name": "a"
                  },
                  "init": {
                    "type": "Literal",
                    "value": 42
                  }
                }
              ],
              "kind": "var"
            }
             */

            /**
             * "declarations-0.init.type" vérifie la valeur nodeA.declarations[0].init.type
             *
             * Le "-0" de "declarations-0" signifie qu'on recherche init.type dans le
             * premier élement (index 0) du tableau déclarations
             *
             * checkAST renvoie la valeur du dernier noeud tester
             */




            //console.log(contentNodeA) // 42
        }
        else
        {
            errors.push(t.errMsg);
        }

    }
    else
    {
        errors.push(t.errMsg);
    }







    /////// END SCRIPT ///////

    // Check for error
    if (errors.length === 0 && warnings.length === 0)
    {
        resultat = true;
    }
    else
    {
        resultat = false;
    }






    console.log('TITLE:', 'exo1 de la catégorie Demo');
    console.log('RESULTAT:', resultat);
    console.log('ERROR:', errors);
    console.log('----------------');
}

module.exports = exec;