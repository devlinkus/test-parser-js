/* default

 */

var scriptIsChild = false;
if(tools.verifSkel())
{
    var head = tools.getTag('head');
    if(typeof head[0] === 'object')
    {
        var src = $('script[src="script.js"]').get();
        if (src.length !== 1)
        {
            errors.push('Avez-vous bien mis le lien vers le fichier script.js ?');
        }
        else
        {
            scriptIsChild = true;
        }
    }
}
errors = tools.errors


if (scriptIsChild)
{

    var isOk = true;

    let myAST = tools.AST.raw;
    let t = new ASTTool(myAST, ['start', 'end', 'raw']);

    // if(!t.checkModel(globalHTMLInit, raw['index.html']))
    // {
    //     errors.push(t.errMsg)
    // }
    // else
    // {
    //     // code here
    // }

    // or here

    /*
    On a copier/coller le code de ./Cours/Demo/js_check/exo1.js
    (entre les commentaires START SCRIPT et END SCRIPT)

    IMPORTANT!!

    pour que le script fonctionne, il faut ajouter
    ASSTool (./class/ASTTool.js) dans l'onglet Initialisation.

    Penser à supprimer la dernière ligne
    (module.exports = ASTTool;) du fichier.

     */

    let nodeA = t.hasVarDeclaration('a');
    if(nodeA)
    {
        let contentNodeA = t.checkAST([
            ['declarations-0.init.type', "Vous la variable a, vous devez afficher un littérale", 'Literal'],
            ['declarations-0.init.value', "Vous n'avez pas assigné la bonne valeur à la variable a", 42],
        ], nodeA);
        if(contentNodeA)
        {
            // on a terminé
        }
        else
        {
            errors.push(t.errMsg);
        }

    }
    else
    {
        errors.push(t.errMsg);
    }



}

// Check for error
if (errors.length === 0 && warnings.length === 0)
{
    resultat = true;
}
else
{
    resultat = false;
}

